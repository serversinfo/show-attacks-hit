#include <csgo_colors>
#define VERSION "1.0.87"
public Plugin myinfo =
{
	name		= "Show attacks",
	author		= "ShaRen",
	description	= "Показывает когда КТ атакует Т",
	version		= VERSION,
	url			= "servers-info.ru"
}

bool g_bShowed[MAXPLAYERS+1];
//float g_fHurtTime[MAXPLAYERS+1][MAXPLAYERS+1];
int g_iCount[MAXPLAYERS+1][MAXPLAYERS+1];
Handle hTimer[MAXPLAYERS+1][MAXPLAYERS+1];

public void OnPluginStart() {
	HookEvent("player_hurt", Event_player_hurt);
	HookEvent("round_start", Event_round_start, EventHookMode_PostNoCopy);
}

public Action Event_round_start(Event e, const char[] name, bool dontBroadcast)
{
	for(int i=1; i<=MaxClients; i++)
		g_bShowed[i] = false;
}

public Action Event_player_hurt(Event e, const char[] name, bool dontBroadcast)
{
	int iClient = GetClientOfUserId(e.GetInt("userid"));
	if (GetClientTeam(iClient) == 3) {			// скажем КТ что они могут купить HealthShot
		int iAttacker = GetClientOfUserId(e.GetInt("attacker"));
		if (iAttacker && GetClientTeam(iAttacker) == 2) {
			g_bShowed[iClient] = true;
			CGOPrintToChat(iClient, "{GREEN}Чтобы купить HealthShot и подлечиться, пиши {RED}sh hs{GREEN} или {RED}sh hs 4 {GREEN} чтобы купить сразу 4");
		}

		return Plugin_Continue;
	}
	int iAttacker = GetClientOfUserId(e.GetInt("attacker"));
	if (!iAttacker || GetClientTeam(iAttacker) != 3)
		return Plugin_Continue;
	DataPack pack;
	if (hTimer[iAttacker][iClient] != null) {
		KillTimer(hTimer[iAttacker][iClient]);
		hTimer[iAttacker][iClient] = null;
	}
	g_iCount[iAttacker][iClient]++;
	hTimer[iAttacker][iClient] = CreateDataTimer(2.0, Send, pack);
	pack.WriteCell(iClient);
	pack.WriteCell(iAttacker);
	switch (e.GetInt("hitgroup")) {
		case 1: {
			CGOPrintToChat(iAttacker, "Попал игроку {GREEN}%N в {RED}голову", iClient);
			CGOPrintToChat(iClient, "{GREEN}%N {DEFAULT}попал по тебе в {RED}голову", iAttacker);
			pack.WriteString("голову");
		} case 2: {
			CGOPrintToChat(iAttacker, "Попал игроку {GREEN}%N в {BLUE}грудь", iClient);
			CGOPrintToChat(iClient, "{GREEN}%N {DEFAULT}попал по тебе в {BLUE}грудь", iAttacker);
			pack.WriteString("грудь");
		} case 3: {
			CGOPrintToChat(iAttacker, "Попал игроку {GREEN}%N в {BLUE}живот", iClient);
			CGOPrintToChat(iClient, "{GREEN}%N {DEFAULT}попал по тебе в {BLUE}живот", iAttacker);
			pack.WriteString("живот");
		} case 4, 5: {
			CGOPrintToChat(iAttacker, "Попал игроку {GREEN}%N в {BLUE}руку", iClient);
			CGOPrintToChat(iClient, "{GREEN}%N {DEFAULT}попал по тебе в {BLUE}руку", iAttacker);
			pack.WriteString("руку");
		} case 6, 7: {
			CGOPrintToChat(iAttacker, "Попал игроку {GREEN}%N в {BLUE}ногу", iClient);
			CGOPrintToChat(iClient, "{GREEN}%N {DEFAULT}попал по тебе в {BLUE}ногу", iAttacker);
			pack.WriteString("ногу");
		} default : pack.WriteString(" ");
	}
	char sWeapon[31];
	e.GetString("weapon", sWeapon, sizeof(sWeapon));
	pack.WriteString(sWeapon);
	return Plugin_Continue;
}

public Action Send(Handle timer, DataPack pack)
{
	char sHit[128], sWeapon[128];
	pack.Reset();
	int iClient = pack.ReadCell();
	int iAttacker = pack.ReadCell();
	hTimer[iAttacker][iClient] = null;
	pack.ReadString(sHit, sizeof(sHit));
	pack.ReadString(sWeapon, sizeof(sWeapon));
	if (g_iCount[iAttacker][iClient] == 1)
		CGOPrintToChatAll("{GREEN}%N {LIGHTPURPLE}ранил {GREEN}%N {LIGHTPURPLE}в {BLUE}%s {LIGHTPURPLE}из {GREEN}%s", iAttacker, iClient, sHit, sWeapon);
	else CGOPrintToChatAll("{GREEN}%N {LIGHTPURPLE}стрелял по {GREEN}%N {LIGHTPURPLE} %i раза подряд", iAttacker, iClient, g_iCount[iAttacker][iClient]);
	g_iCount[iAttacker][iClient] = 0;
}
 
public void OnClientDisconnect(int client)
{
	for(int i=1; i<MAXPLAYERS; i++)
		if (hTimer[client][i] != null) {
			KillTimer(hTimer[client][i]);
			hTimer[client][i] = null;
			g_iCount[client][i] = 0;
		} else if (hTimer[i][client] != null) {
			KillTimer(hTimer[i][client]);
			hTimer[i][client] = null;
			g_iCount[i][client] = 0;
		}
}